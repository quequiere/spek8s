# Spe K8S

This project deploy with helm a HA WordPress website for Supinfo.

By LARIBIERE Bruno - 298261



## Schema

![](.\assets\schema.png)

## Server condition

You need to have nfs helper to mount nfs volume. Install it on each node with

```
sudo apt install nfs-common
```



## Installation

Open command line in spek8s-helm then:

```
helm install spek8s-test .
```



## Full Delete

```
helm delete spek8s-test
kubectl delete -f crds\mcrouter-operator.yaml
kubectl delete pvc -l "app=nfs-server-provisioner"
kubectl delete vitesstoponodes --all
kubectl delete pvc -l "app=vitess"
```

So this should be empty:

```
kubectl get all --all-namespaces
```



## Project Details [FR]

### ./docker-images

------

##### php-fpm

Contient une image php-fpm (php7) à laquelle est rajoutée les modules nécessaires pour la communication SQL ainsi que le cache partagé memcached.

Cette image est hébergée sur dockerhub

##### wordpress

Cette image contient les fichiers nécessaires à l'installation de wordpress. Elle est utilisée uniquement pour installer wordpress. Pas pour l'exécuter.

Les fichiers qui composent l'image sont dans le sous dossier assets.

- test-session.php: permet de tester mcrouter
- wordpress.zip: une fois décompressé dans un PVC contient tous les fichiers nécessaires à wordpress
- wp-config.php: contient les urls de connexion vers la base de donnée. Les identifiants seront surchargés par les variables d'environnements de php-fpm



### ./spek8s-helm

------

Chart de l'ensemble du projet.

##### ./spek8s-helm/charts

Contient les dépendances helm, on va retrouver: 

- la partie nfs pour le partage des pvc en nfs.
- Le helm d'installation pour ingress
- Puis enfin le helm pour vitess

Bien noter que ces dépendances sont référencées dans Chart.yaml



##### ./spek8s-helm/crds

Contient l'installation de l'opérateur mcrouter. Doit être effectué en priorité maximale afin que les manifests de notre package helm puisse correctement reconnaitre l'opérateur comme existant.

##### ./spek8s-helm/values.yaml

Contient les variables d'installation, pour nfs-provider, vitess, mais surtout celles nécessaire à notre propre paquet helm.

> Info supplémentaire: La variable "ipserver" permet de renseigner à metallb l'ip à utiliser pour le load balancing.



##### ./spek8s-helm/templates

Contient l'ensemble des manifestes nécessaire à l'installation de notre paquet helm.



###### 0-metallb-configmap.yaml

Décrit la configuration de metallb



###### 0-metallb-namespace.yaml

Création de l'espace d'environnement pour metallb



###### 0-metallb-secret.yaml

Gère le secret pour le memberlist de metallb (obligatoire pour son fonctionnement)



###### 0-nginx-config.yaml

Server Nginx. On y précise notamment la connexion avec php-fpm pour exécution des fichier php. 



###### 0-phpini-conf.yaml

Configuration de php-fpm, utilisée pour définir où php doit aller chercher pour le partage de session avec mcrouter



###### 0-pvc.yaml

PVC qui sera partagé entre les pods. Est montée en RWM, en mode NFS grâce au nfs provisionner.



###### 0-secrets.yaml

Définit les secrets pour la connexion à la base de données.



###### 1-metallb.yaml

Installation de metallb en mode baremetal



###### 2-mcrouter.yaml & 2-mcrouter-svc.yaml

Installation de mcrouter avec déclaration des services en mode headless



###### 3-wordpress-install-job.yaml

Avec notre image custom permet d'installer wordpress dans le pvc bindé sur les autres pods



###### 4-php_fpm.yaml & 4-php_fpm-svc.yaml

Mis en place de php-fpm et de son cluster ip



###### 5-nginx.yaml & 5-nginx-svc.yaml

Mise en place du serveur nginx et de son service cluster ip



###### 6-ingress.yaml

Mise en place de l'exposition ingress en lien avec le serveur nginx précédemment déployé





## Test session

You can test session cache to see if php cache share is working, with;

http://ip-adress/test-session.php

## Command Help

View output before install

```
helm install spek8s-test . --dry-run --debug
```

Verify helm format

```
helm lint .
```

