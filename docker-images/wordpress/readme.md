# Wordpress image

This image container wordpress installer with env username password for database (wp-config.php) + test-session.php

```
docker build -t quequiere/wordpress-sql-env .

docker push quequiere/wordpress-sql-env
```



Wordpress will be ready to /wordpress-ready folder. You just need to copy it in your pvc



### View image content

```
docker run -it --entrypoint /bin/sh quequiere/wordpress-sql-env
```

